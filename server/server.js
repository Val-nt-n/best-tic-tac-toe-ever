const io = require('socket.io')();

var players = [];
var games = [];

io.on('connection', (client) => {
  let clientName = '';

  client

    .on('tapePseudo', (pseudo) => {
    client.emit('isTakenPseudo', players[pseudo] !== undefined);
  })

    .on('login', (pseudo) => {
      if (players[pseudo] !== undefined) {
        client.emit('isTakenPseudo', true);
        return false;
      }

      players[pseudo] = {
          socket: client,
          free: true,
          game: null,
          symbol: null
      };
      clientName = pseudo;
      client.emit('login', true);

      freePlayers(client).forEach(pseudo => {
        players[pseudo].socket.emit('players', freePlayers(players[pseudo].socket));
      });
  })

    .on('getFreePlayers', () => {
        client.emit('players', freePlayers(client));
  })
  
    .on('inviteRival', (name) => {
      if (players[name] !== undefined) {
        players[name].socket.emit('rsvp', clientName);
      }
    })

      .on('rsvp', ({accept, rival}) => {
        if (accept) {
          players[clientName].rival = rival;
          players[rival].rival = clientName;

          players[clientName].socket.emit('setRival', rival);
          players[rival].socket.emit('setRival', clientName);

          const uniqueKey = Math.random().toString(36).substr(2, 9);
          games[uniqueKey] = {
            players: {
              1: clientName,
              2: rival
            },
            state: []
          };

          players[clientName].symbol = 'X';
          players[rival].symbol = 'O';

          players[clientName].socket.emit('setSymbol', players[clientName].symbol);
          players[rival].socket.emit('setSymbol', players[rival].symbol);

          players[clientName].game = uniqueKey;
          players[rival].game = uniqueKey;

          players[clientName].socket.emit('yourTurn', games[uniqueKey].state);
          players[rival].socket.emit('rivalTurn');
        } else {
          players[clientName].socket.emit('rsvp', null);
        }
    })

      .on('stroke', (i) => {
        if (i > -1 && i < 9 && !games[players[clientName].game].state[i]) {
          games[players[clientName].game].state[i] = players[clientName].symbol; 

          client.emit('rivalTurn');
          players[players[clientName].rival].socket.emit('yourTurn', games[players[clientName].game].state);
        }
    })

      .on('disconnect', () => {
        let key = Object.keys(players).indexOf(clientName);
        if (key > -1) {
          players.splice(key, 1);
        }
    });
});

function freePlayers(client) {
  tmp = [];
  for (var name in players) {
      if (players[name].free && players[name].socket !== client) {
          tmp.push(name);
      }
  }
  return tmp;
}

const port = 8000;
io.listen(port);
console.log('listening on port ', port);