import React from 'react';
import Square from './Square';
import './style/board.css';

class Board extends React.Component {
    renderSquare(i) {
      return (
        <Square 
            value={this.props.squares[i]} 
            onClick={() => this.props.onClick(i)}
            key={i}
        />
      );
    }
  
    render() {
        let num = 0;
        let rows = [];
        for (let i = 0; i < 3; i++) {
            let squares = [];
            for (let y = 0; y < 3; y++) {
                squares.push(this.renderSquare(num++));
            }
            rows.push(<div className="board-row" key={i}>{squares}</div>);
        }

        return (
            <div>
                {rows}
            </div>
        );
    }
}

export default Board;