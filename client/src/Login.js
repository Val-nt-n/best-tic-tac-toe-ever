import React from 'react';
import './style/login.css';

class Login extends React.Component {
    alreadyTaken() {
        return this.props.pseudoAlreadyTaken
            ? <p className="red">Le pseudo est déjà utilisé.</p>
            : '';
    }

    tape(event) {
        this.props.tapePseudo(event.target.name, event.target.value);
    }

    render() {
        return (
            <div id="login">
                <h2>Quel est votre pseudo ?</h2>
                <input type="text" name="player" placeholder="Votre pseudo" onChange={this.tape.bind(this)} /><br />
                <button onClick={this.props.login} disabled={this.props.pseudoAlreadyTaken}>Go!</button>
                {this.alreadyTaken()}
            </div>
        )
    }
}

export default Login;