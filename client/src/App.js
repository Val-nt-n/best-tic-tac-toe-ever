import React from 'react';

import './style/app.css';

import Game from './Game';

class App extends React.Component {
    render() {
        return (
            <div id="frame">
                <Game />
            </div>
        )
    };
}

export default App;