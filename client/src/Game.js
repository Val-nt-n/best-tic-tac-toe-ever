import React from 'react';
import socketIOClient from "socket.io-client";
import './style/game.css';

import Board from './Board';
import Login from './Login';

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            player: null,
            takenPseudo: false,
            logged: false,
            socket: socketIOClient("http://localhost:8000", {
                'sync disconnect on unload': true }),
            players: null,
            rival: null,
            inviteBy: null,
            myTurn: null,
            symbol: null,
            gameState: []
        }
    }

    componentWillMount() {
        this.state.socket
            .on('isTakenPseudo', (taken) => {
            this.setState({takenPseudo: taken});
        })
            .on('login', (logged) => {
                this.setState({logged: logged});
            if (logged) {
                this.state.socket.emit('getFreePlayers');
            }
        })
            .on('players', (players) => {
                this.setState({players: players});
        })
            .on('rsvp', (player) => {
                this.setState({inviteBy: player});
        })
            .on('setRival', (rival) => {
                this.setState({rival: rival});
        })
            .on('setSymbol', (symbol) => {
                this.setState({symbol: symbol});
        })
            .on('yourTurn', (game) => {
                this.setState({
                    myTurn: true,
                    gameState: game
                });
        })
            .on('rivalTurn', () => {
                this.setState({myTurn: false});
        });
    }

    handleClick(i) {
        if (!this.state.myTurn || this.state.gameState[i] || calculateWinner(this.state.gameState)) {
            return;
        }

        var newGameState = this.state.gameState;
        newGameState[i] = this.state.symbol;
        this.setState({gameState: newGameState})
        this.state.socket.emit('stroke', i);
    }

    jumpTo(step) {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0
        })
    }

    tapePseudo(name) {
        this.setState({
            player: name
        });

        this.state.socket.emit('tapePseudo', name);
    }

    logPlayer() {
        this.state.socket.emit('login', this.state.player);
    }

    chooseRival(name) {
        this.state.socket.emit('inviteRival', name);
    }

    rsvp(accept) {
        this.state.socket.emit('rsvp', {
            accept: accept,
            rival: this.state.inviteBy
        });
    }

    render() {
        let winner = calculateWinner(this.state.gameState);

        if (winner) {
            winner = winner === this.state.symbol
                ? 'Vous avez gagné'
                : this.state.rival + ' a gagné';
        }

        let status = winner
            ? winner
            : 'C\'est à ' + (this.state.myTurn ? 'vous' : this.state.rival);


        var screen = '';
        if (!this.state.logged) {
            screen = (
                <div>
                    <Login 
                        tapePseudo={(player, name) => this.tapePseudo(name)}
                        login={() => this.logPlayer()}
                        pseudoAlreadyTaken={this.state.takenPseudo}
                    />
                </div>
            );
        } else if (this.state.rival) {
            screen = (
                <div className="game">
                    <div className="game-board">
                        <Board
                            squares={this.state.gameState}
                            onClick={(i) => this.handleClick(i)}
                        />
                    </div>
                    <div className="game-info">
                        <div>{status}</div>
                    </div>
                </div>
            );
        } else if (this.state.inviteBy) {
            screen = (
                <div className="game">
                    <h1>{this.state.inviteBy} veut faire une partie avec vous</h1>
                    <p>
                        <button onClick={() => this.rsvp(true)}>Accepter</button>
                        <button onClick={() => this.rsvp(false)}>Refuser</button>
                    </p>
                </div>
            );
        } else if (this.state.players) {
            screen = this.state.players.length
                ? (
                    <div className="game">
                        <h1>Choisissez votre adversaire :</h1>
                        <ul className="players-list">
                            {this.state.players.map((player, i) => 
                                <li 
                                    className="player" 
                                    key={i}
                                    onClick={() => this.chooseRival(player)}
                                >
                                    {player}
                                </li>
                            )}
                        </ul>
                    </div>
                ) :
                <p>Il n'y a pas d'autre joueur.</p>;
        }

        return screen;
    }
}

function calculateWinner(squares) {    
    if (!squares) {
        return null;
    }

    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }
    return null;
}

export default Game;